/* Copyright 2017, DSI FCEIA UNR - Sistemas Digitales 2
 *    DSI: http://www.dsi.fceia.unr.edu.ar/
 * Copyright 2017, Diego Alegrechi
 * Copyright 2017, Gustavo Muro
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/

#include "board.h"
#include "board_rs485.h"
#include "key.h"
#include "MKL46Z4.h"
#include "fsl_sim_hal.h"
#include "fsl_lpsci_hal.h"
#include "fsl_clock_manager.h"
#include "fsl_pit_hal.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/
static int32_t timeDown1ms;

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

void PIT_Init(void)
{
    uint32_t frecPerif;

    SIM_HAL_EnableClock(SIM, kSimClockGatePit0);
    PIT_HAL_Enable(PIT);
    CLOCK_SYS_GetFreq(kBusClock, &frecPerif);
    PIT_HAL_SetTimerPeriodByCount(PIT, 1, frecPerif/1000);
    PIT_HAL_SetIntCmd(PIT, 1, true);
    PIT_HAL_SetTimerRunInDebugCmd(PIT, false);
    PIT_HAL_StartTimer(PIT, 1);
    NVIC_ClearPendingIRQ(PIT_IRQn);
    NVIC_EnableIRQ(PIT_IRQn);
}

/*==================[external functions definition]==========================*/

int main(void)
{

    uint8_t i =0,j=0;
    uint8_t   Array_ToggleLEDROJO [20];
    uint8_t   Array_ToggleLEDVERDE[20];
    Array_ToggleLEDROJO[0]= ':';
    Array_ToggleLEDROJO[1]= '0';
    Array_ToggleLEDROJO[2]= '9';
    Array_ToggleLEDROJO[3]= '0';
    Array_ToggleLEDROJO[4]= '1';
    Array_ToggleLEDROJO[5]= 'T';
    Array_ToggleLEDROJO[6]= 10;

    Array_ToggleLEDVERDE[0]= ':';
	Array_ToggleLEDVERDE[1]= '0';
	Array_ToggleLEDVERDE[2]= '9';
	Array_ToggleLEDVERDE[3]= '0';
	Array_ToggleLEDVERDE[4]= '2';
	Array_ToggleLEDVERDE[5]= 'T';
	Array_ToggleLEDVERDE[6]= 10;

    board_init();

    board_rs485_init();

    PIT_Init();

    while(1)
    {
        if (key_getPressEv(BOARD_SW_ID_1)){

            board_rs485_sendByte( Array_ToggleLEDVERDE[i] );
            i++;
            if(i > 6)
            	i=0;
            j=0;
        }

        if (key_getPressEv(BOARD_SW_ID_3)){
        	board_rs485_sendByte( Array_ToggleLEDROJO[j] );
			j++;
			if(j > 6)
				j=0;
			i=0;
        }


        if (board_rs485_isDataAvailable())
        {
            uint8_t dataRec;

            dataRec = board_rs485_readByte();

            if (dataRec == 'E')
                board_setLed(BOARD_LED_ID_ROJO, BOARD_LED_MSG_ON);

            if (dataRec == 'A')
                board_setLed(BOARD_LED_ID_ROJO, BOARD_LED_MSG_OFF);
        }

        if (timeDown1ms == 0)
        {
            timeDown1ms = 200;
            board_setLed(BOARD_LED_ID_VERDE, BOARD_LED_MSG_TOGGLE);
        }
    }
}

void PIT_IRQHandler(void)
{
    PIT_HAL_ClearIntFlag(PIT, 1);

    if (timeDown1ms)
        timeDown1ms--;

    key_periodicTask1ms();
}


/*==================[end of file]============================================*/




